<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


#[AsCommand(
    name: 'app:create-user',
    description: 'Create a new user',
)]
class CreateUserCommand extends Command
{
    private $entityManager;
    private $passwordHasher;
    private $envVarProcessor;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'email of the new user')
            ->addArgument('password', InputArgument::REQUIRED, 'Password of the new user');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([]);
        if ($user instanceof User) {
            $output->writeln('User ID: ' . $user->getId());
            $output->writeln('Email: ' . $user->getEmail());
        }else{
            $email = $input->getArgument('email');
            $plaintextPassword = $input->getArgument('password');

            $user = new User();
            $user->setEmail($email);

            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );

            $user->setPassword($hashedPassword);
            $user->setRoles(['ROLE_USER']);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $output->writeln('User created successfully.');

        }
        return Command::SUCCESS;
    }
}
