<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;


class ProductController extends AbstractController
{
    private $entityManager;
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    #[Route('/api/products', name: 'products_list', methods: ['GET'])]
    public function list(): JsonResponse
    {
        $products = $this->entityManager->getRepository(Product::class)->findAll();

        $productsData = [];

        foreach ($products as $product) {
            $productArray = [
                'id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'created_at' => $product->getCreatedAt()->format('Y-m-d H:i:s'),
                'updated_at' => $product->getUpdatedAt() ? $product->getUpdatedAt()->format('Y-m-d H:i:s') : null,
            ];

            $productsData[] = $productArray;
        }

        $responseData = ['data' => $productsData];

        return new JsonResponse($responseData, Response::HTTP_OK);
    }

    #[Route('/api/product', name: 'product_add', methods: ['POST'])]
    public function productAdd(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        try {
            // Validate and process the received data
            $sku = $data['sku'] ?? null;
            $name = $data['name'] ?? null;
            $description = $data['description'] ?? null;

            $this->validateProductData($sku, $name);

            $existingProduct = $this->entityManager->getRepository(Product::class)->findOneBy(['sku' => $sku]);
            if ($existingProduct) {
                $errorResponse = ['error' => 'already exists SKU: ' . $sku];
                return new JsonResponse($errorResponse, Response::HTTP_CONFLICT);
            }

            // Create the new product
            $product = new Product();
            $product->setSku($sku);
            $product->setName($name);
            $product->setDescription($description);

            $this->entityManager->persist($product);
            $this->entityManager->flush();

            $productJson = $this->serializer->serialize($product, 'json');

            $responseData = ['message' => 'Product added successfully', 'data' => json_decode($productJson, true)];
            return new JsonResponse($responseData, Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            // Capturar la excepción específica y devolver un JsonResponse
            return new JsonResponse(json_decode($exception->getMessage(), true), Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/api/products', name: 'products_update', methods: ['PUT'])]
    public function productsUpdate(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        // Validate and process the received data
        $productsData = $data['products'] ?? [];
        
        if (empty($productsData)) {
            $errorResponse = ['error' => 'No products data provided'];
            return new JsonResponse($errorResponse, Response::HTTP_BAD_REQUEST);
        }

        $updatedProducts = [];
        $this->entityManager->beginTransaction();

        try {

            foreach ($productsData as $productData) {
                $sku = $productData['sku'] ?? null;
                $name = $productData['name'] ?? null;
                $description = $productData['description'] ?? null;

                $this->validateProductData($sku, $name, true);

                // Check if SKU or name do not exist
                if (empty($sku) || empty($name)) {
                    $errorResponse = ['error' => 'SKU and name are required for each product'];
                    return new JsonResponse($errorResponse, Response::HTTP_BAD_REQUEST);
                }

                // Check if the product with the given SKU exists
                $product = $this->entityManager->getRepository(Product::class)->findOneBy(['sku' => $sku]);
                if (!$product) {
                    $errorResponse = ['error' => 'Product not found for SKU: ' . $sku];
                    return new JsonResponse($errorResponse, Response::HTTP_NOT_FOUND);
                }

                // Update the product
                $product->setName($name);
                $product->setDescription($description);

                $this->entityManager->persist($product);
                $updatedProducts[] = $product;
            }

            $this->entityManager->flush();
            $this->entityManager->commit();

            // Respond with the updated product data
            $responseData = ['message' => 'Products updated successfully', 'data' => $updatedProducts];
            $responseJson = $this->serializer->serialize($responseData, 'json');

            return new JsonResponse(json_decode($responseJson, true), Response::HTTP_OK);

        } catch (\Exception $exception) {
            $this->entityManager->rollback();

            $errorResponse = ['error' => $exception->getMessage()];
            return new JsonResponse($errorResponse, Response::HTTP_BAD_REQUEST);
        }
    }


    function validateProductData($sku, $name, $manyProduct=false) {
        // Check if SKU or name do not exist
        if (empty($sku) || empty($name)) {
            $msgRequired = $manyProduct ? 'SKU and name are required for each product' : 'SKU and name are required';
            $errorResponse = ['error' => $msgRequired];
            throw new \Exception(json_encode($errorResponse));
        }

        $validateList = [];
    
        // Check length SKU
        if (mb_strlen($sku) > 50) {
            $validateList[] = ['error' => 'SKU is too long (maximum 50 characters).'];
        }
        
        // Check length name
        if (mb_strlen($name) > 250) {
            $validateList[] = ['error' => 'Name is too long (maximum 250 characters).'];
        }
        
        if (count($validateList) > 0) {
            throw new \Exception(json_encode($validateList));
        }
    
    }
    
}
