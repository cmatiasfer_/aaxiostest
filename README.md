# AAXIOS TEST

## Prerequisites
- PHP 8.0 or higher
- Composer
- PostgreSQL
- OpenSSL
- Postman (Import Collection)

## Initial Setup
1. **Clone this repository to your local machine:**
    ```bash
    git clone https://gitlab.com/cmatiasfer_/aaxiostest.git
    ```

2. **Install project dependencies:**
    ```bash
    composer install
    ```

3. **Configure environment variables:**
    - Create a file named `.env.local`.
    - Add the necessary configuration, including the database username and password.

4. **Generate public and private keys for JWT:**
    ```bash
    php bin/console lexik:jwt:generate-keypair
    ```

5. **Create the database and apply migrations:**
    ```bash
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    ```

6. **Create a user via command line:**
    ```bash
    php bin/console app:create-user <email> <password>
    ```
    *Note: Only create one user. If a user already exists, it will display the email in the database.*

These steps ensure that your Symfony project is set up and ready to run. Adjust the configuration details according to your specific needs.

---


# Endpoints

## 1. Add a New Product

- **Route:** `/api/product`
- **Method:** `POST`
- **Description:** Allows adding a new product.
- **Example JSON Request:**
    ```json
    {
      "sku": "ABC123",
      "name": "Product Name",
      "description": "Optional product description"
    }
    ```
- **Successful Response:**
    ```json
    {
      "message": "Product added successfully",
      "data": {
        "id": 1,
        "sku": "ABC123",
        "name": "Product Name",
        "description": "Optional product description",
        "created_at": "2022-01-01 12:00:00",
        "updated_at": null
      }
    }
    ```
- **Possible Errors:**
    - Status Code: `400` - SKU and name are required.
    - Status Code: `409` - SKU already exists.

## 2. Update Products

- **Route:** `/api/products`
- **Method:** `PUT`
- **Description:** Allows updating multiple products at once.
- **Example JSON Request:**
    ```json
    {
      "products": [
        {
          "sku": "ABC123",
          "name": "New Product Name",
          "description": "New optional description"
        },
        {
          "sku": "XYZ789",
          "name": "Another New Product",
          "description": "Another new description"
        }
      ]
    }
    ```
- **Successful Response:**
    ```json
    {
      "message": "Products updated successfully",
      "data": [
        {
          "id": 1,
          "sku": "ABC123",
          "name": "New Product Name",
          "description": "New optional description",
          "created_at": "2022-01-01 12:00:00",
          "updated_at": "2022-01-02 14:30:00"
        },
        {
          "id": 2,
          "sku": "XYZ789",
          "name": "Another New Product",
          "description": "Another new description",
          "created_at": "2022-01-03 09:45:00",
          "updated_at": "2022-01-04 11:20:00"
        }
      ]
    }
    ```
- **Possible Errors:**
    - Status Code: `400` - SKU and name are required.
    - Status Code: `404` - Product not found for SKU.

## 3. Get List of Products

- **Route:** `/api/products`
- **Method:** `GET`
- **Description:** Retrieves a list of all products.
- **Successful Response:**
    ```json
    {
      "data": [
        {
          "id": 1,
          "sku": "ABC123",
          "name": "Product Name",
          "description": "Optional product description",
          "created_at": "2022-01-01 12:00:00",
          "updated_at": "2022-01-02 14:30:00"
        },
        {
          "id": 2,
          "sku": "XYZ789",
          "name": "Another Product",
          "description": "Another optional description",
          "created_at": "2022-01-03 09:45:00",
          "updated_at": null
        }
        // Other products...
      ]
    }
    ```
- **Possible Errors:**
    - Status Code: `404` - No products found.
